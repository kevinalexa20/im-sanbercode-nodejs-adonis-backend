"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sapa = exports.biodata = void 0;

var sapa = function sapa(nama) {
  return "Halo selamat pagi, ".concat(nama, "!");
};

exports.sapa = sapa;

var biodata = function biodata(name, domisili, umur) {
  return {
    name: name,
    domisili: domisili,
    umur: umur
  };
};

exports.biodata = biodata;