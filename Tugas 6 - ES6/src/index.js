
import { sapa, biodata } from './libs/soal'

const myArgs = process.argv.slice(2);
const command = myArgs[0]

switch (command) {
  case 'sapa':
    let nama = myArgs[1]

    console.log(sapa(nama));
    break;

  case 'biodata':
    const params = myArgs.slice(1);

    let [name, domisili, umur] = params

    console.log(biodata(name, domisili, umur))

    break;
  default:
    break;
}
