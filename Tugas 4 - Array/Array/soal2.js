function balikKata(kata) {
  var output = "";
  var panjangString = kata.length;

  for (var a = panjangString - 1; a >= 0; a--) {
    output += kata[a]
  }

  return output;
}


console.log(balikKata("SanberCode"))
//Output: edoCrebnaS

console.log(balikKata("racecar"))
//Output: racecar

console.log(balikKata("kasur rusak"))
//Output: kasur rusak

console.log(balikKata("haji ijah"))
//Output: haji ijah

console.log(balikKata("I am Sanbers"))
//Output: srebnaS ma I