function naikAngkot(listPenumpang) {
  var rute = ['A', 'B', 'C', 'D', 'E', 'F'];

  var output = [];
  for (var a = 0; a < listPenumpang.length; a++) {
    var penumpangSekarang = listPenumpang[a]
    var obj = {
      penumpang: penumpangSekarang[0],
      naik: penumpangSekarang[1],
      tujuan: penumpangSekarang[2]
    }

    var bayar = (rute.indexOf(penumpangSekarang[2]) - rute.indexOf(penumpangSekarang[1])) * 2000

    obj.bayar = bayar
    output.push(obj)
  }
  return output
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]