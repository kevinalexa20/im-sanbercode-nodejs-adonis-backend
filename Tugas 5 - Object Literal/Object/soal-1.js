

function arrayToObject(arr) {
  for (var i = 0; i < arr.length; i++) {

    var thisYear = (new Date()).getFullYear();

    var personArr = arr[i];

    var objPerson = {
      firstName: personArr[0],
      lastName: personArr[1],
      gender: personArr[2],
      age: personArr[3]
    }

    if (!personArr[3] || personArr[3] > thisYear) {
      objPerson.age = "invalid Birth Year"
    } else {
      objPerson.age = thisYear - personArr[3];
    }

    var fullname = objPerson.firstName + "" + objPerson.lastName
    console.log(`${i + 1} . ${fullname} : `, objPerson)

  }
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)