const printTriangle = (num) => {
  let str = ""
  for (let x = 1; x <= num; x++) {

    for (let y = 0; y < x; y++) {
      str += "#";
    }
    str += "\n";
  }
  console.log(str);
}
printTriangle(5);
