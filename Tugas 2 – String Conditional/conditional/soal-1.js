var nama = "John"
var peran = "Guard"

if (nama == "John" && peran == "Guard") {
  console.log("Selamat datang di Dunia Werewolf, John. " +
    "Halo Guard John, kamu akan membantu melindungi temanmu dari serangan werewolf.");
} else if (nama == "John" && peran == "Werewolf") {
  console.log("Selamat datang di Dunia Werewolf, John. " +
    "Halo Werewolf John, Kamu akan memakan mangsa setiap malam!");
} else if (nama == "John" && peran == "Penyihir") {
  console.log("Selamat datang di Dunia Werewolf, John. " +
    "Halo Penyihir John, kamu dapat melihat siapa yang menjadi werewolf!");
} else if (nama == "John" && peran == "") {
  console.log("Halo John, Pilih peranmu untuk memulai game!");
} else {
  console.log("Nama harus diisi!");
}